from unigametheory.base.player import Player
from unigametheory.game.sample import SampleGame
import unigametheory.game
from circuits import Debugger, Component, Event, Timer
import logging as log
from argparse import ArgumentParser
import inspect
import cProfile


class stop_it(Event):
    pass


class Stop(Component):
    def stop_it(self, player):
        print("Received the signal")
        player.stop()


def print_sig(*args, **kwargs):
    print("Received a signal")


def get_class(name):
    for cls_name, cls in inspect.getmembers(unigametheory.game, inspect.isclass):
        if cls_name == name:
            return cls
    raise Exception("No such class: " + name)


def argparser():
    parser = ArgumentParser(description="Game theory bot")
    # Any of:
    # - Prisoner's dilemma: "ws://dmc.alepoydes.com:3012"
    # - Chicken: "ws://dmc.alepoydes.com:3013"
    # - Common good: "ws://dmc.alepoydes.com:3014"
    parser.add_argument("host", help="Target host URI")
    parser.add_argument("name", help="Target class")
    parser.add_argument("--login", help="Username for login")
    parser.add_argument("--password", help="Password for the login")
    return parser


def main(args):
    URL = args.host
    cls = get_class(args.name)
    fmt = "%(asctime)s => %(message)s"
    log.basicConfig(format=fmt, level=log.INFO)
    # for sig in [signal.SIGINT, signal.SIGTERM]:
    #    signal.signal(sig, print_sig)
    creds = {"login": args.login, "password": args.password}
    player = Player(URL, cls, creds)
    # Debugger().register(player)
    # Stop().register(player)
    # Timer(60, stop_it(player)).register(player)
    player.run()


if __name__ == "__main__":
    parser = argparser()
    main(parser.parse_args())
