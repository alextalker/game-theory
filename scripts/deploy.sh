#!/usr/bin/env bash

set -e

cd /tmp
cp -r /project .
cd project/
python setup.py build_ext --inplace