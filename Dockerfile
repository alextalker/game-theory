FROM python:3

WORKDIR /tmp/
ADD requirements.txt .

RUN pip install $(cat requirements.txt)

CMD [ '/bin/bash' ]