from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

ext = [Extension("*", ["unigametheory/**/*.py"])]

setup(
    ext_modules = cythonize(ext, compiler_directives={'language_level': 3})
)