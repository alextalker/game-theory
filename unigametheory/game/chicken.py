from ..base.game import Game


class ChickenRapoportGame(Game):
    def turn(self, opts):
        if opts is None:
            # TODO: Adapt for different matrix
            move = 0
        else:
            moves = opts["moves"]
            other = (self.hand - 1) % 2
            move = moves[other]
        return move


class ChickenPavlovGame(Game):
    def turn(self, opts):
        if opts is None:
            # TODO: Adapt for different matrix
            return 0
        else:
            # TODO: Betray with certain probability
            moves = opts["moves"]
            score = self.params["payoff"][moves[0]][moves[1]]
            other = (self.hand - 1) % 2
            if score[self.hand] < score[other]:
                return moves[other]
            else:
                return moves[self.hand]


class ChickenRecklessGame(Game):
    def turn(self, opts):
        if opts:
            moves = opts["moves"]
            score = self.params["payoff"][moves[0]][moves[1]]
            other = (self.hand - 1) % 2
            if score[other] and (abs(score[self.hand] / score[other]) < 1.5):
                return moves[other]
            else:
                return 1
        return 1


class ChickenInsaneGame(Game):
    def turn(self, opts):
        if opts is None:
            # TODO: Adapt for different matrix
            return 0
        else:
            # TODO: Betray with certain probability
            moves = opts["moves"]
            score = self.params["payoff"][moves[0]][moves[1]]
            other = (self.hand - 1) % 2
            if score[self.hand] < score[other]:
                return moves[other]
            else:
                return 1
