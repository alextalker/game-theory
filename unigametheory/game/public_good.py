from ..base.game import Game


class PublicGoodLazyGame(Game):
    def turn(self, opts):
        if opts:
            players_in = sum(opts["moves"])
            players_out = len(opts["moves"]) - players_in
            if not players_in:
                return 1
            if players_out > players_in:
                return 1
            our_move = opts["moves"][self.hand]
            our_income = self.params["income"][players_in - our_move]
            win_income = self.params["income"][players_in]
            return int(our_income != win_income)
        return 1
