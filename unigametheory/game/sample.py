from ..base.game import Game
import random


class SampleGame(Game):
    def turn(self, opts):
        move = random.choice([0, 1])
        return move
