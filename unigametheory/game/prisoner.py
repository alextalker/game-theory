from ..base.game import Game
from random import random, seed, getstate, setstate


class PrisonerRapoportGame(Game):
    def turn(self, opts):
        if opts is None:
            # TODO: Adapt for different matrix
            move = 1
        else:
            # TODO: Betray with certain probability
            moves = opts["moves"]
            other = (self.hand - 1) % 2
            move = moves[other]
        return move


class PrisonerPavlovGame(Game):
    def turn(self, opts):
        if opts is None:
            # TODO: Adapt for different matrix
            return 1
        else:
            # TODO: Betray with certain probability
            moves = opts["moves"]
            score = self.params["payoff"][moves[0]][moves[1]]
            other = (self.hand - 1) % 2
            if score[self.hand] < score[other]:
                return moves[other]
            else:
                return moves[self.hand]


class PrisonerPavlovForgivingGame(Game):
    def turn(self, opts):
        if opts is None:
            # TODO: Adapt for different matrix
            seed(self.game)
            self.state = getstate()
            return 1
        else:
            setstate(self.state)
            betray = random() <= self.params["termination_probability"]
            self.state = getstate()
            if betray:
                return 0
            moves = opts["moves"]
            score = self.params["payoff"][moves[0]][moves[1]]
            other = (self.hand - 1) % 2
            if score[self.hand] < score[other]:
                return moves[other]
            else:
                return moves[self.hand]
