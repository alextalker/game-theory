from circuits import Component
from circuits import ipc
from circuits.web.websockets import WebSocketClient
from enum import Enum
import logging
from pprint import pformat

from .game import handlers
from .util import Serializer, SerializerMixin

LOG = logging.getLogger(__name__)


class PlayerState(Enum):
    CONNECTED = 1
    AUTH = 2
    IN_GAME = 3


class Player(SerializerMixin, Component):
    channel = "player"

    def init(self, url, game, creds, debug=True):
        self.url = url
        self.game = game
        self.state = PlayerState.CONNECTED
        self.creds = creds
        self.debug = debug
        WebSocketClient(self.url, wschannel=self.channel).register(self)

    def connected(self, host, port):
        pass

    def read(self, raw):
        # TODO: Exception handling
        obj = Serializer.decode(raw)
        if not isinstance(obj, dict):
            LOG.error("Unrecoverable error")
            return
        if "state" in obj and obj["state"] == "error":
            LOG.error("Error: %s", pformat(obj))
            return
        # Move auth into "connected" event
        if self.state == PlayerState.CONNECTED:
            self.response(
                self.channel, {"state": "login", "debug": self.debug, **self.creds}
            )
            self.state = PlayerState.AUTH
            return
        if self.state == PlayerState.AUTH:
            self.state = PlayerState.IN_GAME
            return
        if self.state == PlayerState.IN_GAME:
            state = obj["state"]
            channel = "game_{}".format(obj["game"])
            if state == "start":
                LOG.info("Starting game %d", obj["game"])
                game = self.game(obj, self.channel, channel=channel)
                game.register(self)
                return
            handler = handlers[state]
            if handler is not None:
                self.fire(handler(obj), channel)
