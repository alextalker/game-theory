from circuits import Component, Event
from circuits.net.sockets import write

from pprint import pformat
import logging
from .util import SerializerMixin

LOG = logging.getLogger(__name__)


# TODO: Remove
class start(Event):
    pass


class gameover(Event):
    pass


class turnover(Event):
    pass


handlers = {"turnover": turnover, "gameover": gameover}


# Pattern - one game = one component
class Game(SerializerMixin, Component):
    # Stub
    # channel = "game"

    # Instead of start
    def init(self, opts, player, channel):
        self.player = player
        self.game = opts["game"]
        LOG.info("Start of the game %d...", self.game)
        self.hand = opts["hand"]
        self.params = opts["parameters"]
        LOG.debug("Game: %s", pformat(opts))

    def registered(self, component, *args, **kwargs):
        # Make first move
        self._turn()

    def turnover(self, opts):
        LOG.debug("In-game turn... %s", pformat(opts))
        # Make another move
        # TODO: Handle options gracefully
        self._turn(opts)

    def gameover(self, opts):
        LOG.info("Game over...%s", pformat(opts))
        if len(opts["scores"]) > self.hand:
            scores = opts["scores"]
            winner = max(scores)
            our = scores[self.hand]
            LOG.info("Our score is %f our of %f[%s]", our, winner, our == winner)
        # Remove ourselves from the tree
        self.unregister()

    def _turn(self, opts=None):
        self.response(
            self.player,
            {"state": "move", "strategy": self.turn(opts), "game": self.game},
        )
