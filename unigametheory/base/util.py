from circuits.net.sockets import write

import json


class Serializer(object):
    @classmethod
    def encode(cls, obj):
        return json.dumps(obj)

    @classmethod
    def decode(cls, str):
        return json.loads(str)


class SerializerMixin(object):
    def response(self, channel, obj):
        self.fire(write(Serializer.encode(obj)), channel)
